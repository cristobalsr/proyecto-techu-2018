
var cuentas = require('./servCuentas.js')


exports.cargaModulo = function(app) {

//Crear cuenta
app.post('/apitechu/v5/cuentas', function(req, res){
    cuentas.crearCuenta(req,res)
  })
  
  //Eliminar cuenta
  app.delete('/apitechu/v5/cuentas/:_id', function(req, res) {
    cuentas.eliminarCuenta(req, res)
  })
  
  //Crear movimiento
  app.post('/apitechu/v5/movimiento', function(req, res){
    cuentas.crearMovimiento(req, res)
  })
  
  //Eliminar movimiento
  app.delete('/apitechu/v5/movimiento', function(req, res){
    cuentas.eliminarMovimiento(req, res)
  })
  
  // Devuelve las cuentas de un cliente
  app.get('/apitechu/v1/iban/:id', function(req,res){
    cuentas.obtenerCuentas(req, res)
  })
  
  // Dado el iban de una cuenta, consultar los movimientos
  app.get('/apitechu/v1/movimientos', function(req,res){
    cuentas.obtenerMovimientos(req,res)
  })
}