var requestJson = require('request-json')
var urlMlabRaiz = "https://api.mlab.com/api/1/databases/techu2018/collections"
var apiKey = "apiKey=1fa5CiAnKeIRiPcw8OVs9zOcvFJZOQvn"
var conversorDivisa = require('./conversorDivisa.js')
conversorDivisa.inicializa()

exports.crearCuenta = function(req, res) {
    console.log(req.body)
    var data = {
      iban: req.body.iban,
      idcliente: req.body.idcliente,
      saldo: Number(req.body.saldo),
      movimientos:[]
    }
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + apiKey)
    clienteMlab.post("?"+ apiKey, data,  function(errP, resP, bodyP) {
      res.send(bodyP)
    })
}

actualizaSaldo = function(saldo,req, res){
  var data = {
    $set: {
      saldo: saldo
    }
  }
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + apiKey)
  clienteMlab.put("?"+ apiKey, data,  function(errP, resP, bodyP) {
    bodyP["saldo"]= saldo
    res.send(bodyP)
  })    
}

exports.eliminarCuenta = function(req, res) {
    var _id = req.params._id
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas/"+ _id + "?" + apiKey)
    clienteMlab.del('', function(err, resM, body) {
      res.send(body)
    })    
}

exports.crearMovimiento = function(req, res) {
    var iban = req.body.iban
    var date = new Date()
    var data = {
      $push: { movimientos: {
          fecha: date.toDateString(),
          importe: Number(req.body.importe),
          moneda: req.body.moneda,
          id: req.body.id
          
        }
      }
    }
    var query = "?q={'iban':'" + iban + "'}&"
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?"  + apiKey)
    clienteMlab.put(query + apiKey, data,  function(errP, resP, bodyP) {
        var saldo = Number(req.body.saldo) + conversorDivisa.convierte(Number(req.body.importe),req.body.moneda,"EUR")
        actualizaSaldo(saldo,req,res)
    })

}

exports.eliminarMovimiento = function(req, res){
    var iban = req.headers.iban
    var data = {
      $pull: { movimientos: {
          id: req.headers.id
        }
      }
    }
    var query = "?q={'iban':'" + iban + "'}&"
    console.log("Eliminar movimiento:" + query)
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?"  + apiKey)
    clienteMlab.put(query + apiKey, data,  function(errP, resP, bodyP) {
      console.log(errP)
      console.log(bodyP)
      res.send(bodyP)
    })
}

exports.obtenerCuentas = function(req, res){
    var id = req.params.id
    var query = 'q={"idcliente":"' + id + '"}&f={"iban":1,"saldo":1, "_id":1}&'
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + apiKey)
    clienteMlab.get('', function(err, resM, body) {
      if (!err) {
        if (body.length > 0)
          res.send(body)
        else {
          res.status(404).send('Usuario no encontrado')
        }
      }
    })
}

exports.obtenerMovimientos = function(req, res) {
    var idCuenta = req.headers.idcuenta
    var query = 'q={"iban":"' + idCuenta + '"}&f={"movimientos":1}'
    console.log(query)
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&l=1&" + apiKey)
    clienteMlab.get('', function(err, resM, body) {
      //console.log(resM)
      if (!err) {
        if (body.length > 0)
          res.send(body[0])
          //res.send({"nombre":body[0].nombre, "apellidos":body[0].apellidos})
        else {
          res.status(404).send('Cuenta no encontrada')
        }
      }
    })
}