var requestJson = require('request-json')
var urlFixer = "http://data.fixer.io/api/latest?"
var apiKey = "access_key=58e0ebdd0f00479316f78bef41b5f204"

var tablaDivisas

/*
 * Inicializa la tabla de rates a través del API https://fixer.io
 */
exports.inicializa = function (){
    var clienteFixer = requestJson.createClient(urlFixer + apiKey)
    clienteFixer.get('', function(err, resM, body) {
        if (err == null) {
            console.log("Tabla divisas inicializada:" + body)
            tablaDivisas = body
        }
        else {
            console.log("Error inicializando tabla de divisas:" + err)
            tablaDivisas = null
        }
    })
}

/*
 * Conversión de divisas a través del API https://fixer.io
 */
exports.convierte = function (importe, origen, destino){
    console.log("base:" + tablaDivisas.base)
    console.log("tablaDivisas.rates[destino]:" +tablaDivisas.rates[destino])
    if (tablaDivisas.base == origen){
        return importe * tablaDivisas.rates[destino]
    }
    else {
        return importe / tablaDivisas.rates[origen]
    }
}

