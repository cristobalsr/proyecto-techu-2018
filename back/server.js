var express = require('express')
var bodyParser = require('body-parser')
var cors = require('cors')
var requestJson = require('request-json')

var usuarios = require('./usuarios/rutasUsuarios.js')
var cuentas = require('./cuentas/rutasCuentas.js')

var app = express()
var urlMlabRaiz = "https://api.mlab.com/api/1/databases/techu2018/collections"
var apiKey = "apiKey=1fa5CiAnKeIRiPcw8OVs9zOcvFJZOQvn"
var clienteMlab
var port = process.env.PORT || 3000



app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/apitechu/v1', function(req, res)
{
  res.send({"mensaje":"Bienvenido a mi API"})
})

usuarios.cargaModulo(app)
cuentas.cargaModulo(app)


app.listen(port)
console.log("API escuchando en el puerto" + port)