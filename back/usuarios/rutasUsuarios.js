

var usuarios = require('./servUsuarios.js')

exports.cargaModulo = function(app) {

// Obtiene la lista de usuarios
app.get('/apitechu/v5/usuarios', function(req, res) {
    usuarios.listaUsuarios(req,res)
  })
  
  // Obtiene nombre y apellidos de un usuario a partir de su id.
  app.get('/apitechu/v5/usuarios/:id', function(req, res) {
    usuarios.obtenerUsuario(req, res)
  })
  
  //Crear nuevo usuario
  app.post('/apitechu/v5/usuarios', function(req,res){
    usuarios.crearUsuario(req, res)
  })
  
  //Modificar usuario
  app.put('/apitechu/v5/usuarios', function(req,res){
    usuarios.modificarUsuario(req, res)
  })
  
  //Eliminar usuario
  app.delete('/apitechu/v5/usuarios/:_id', function(req, res) {
    usuarios.eliminarUsuario(req, res)
  })
  
  //Hace login de un usuario
  app.post('/apitechu/v5/login', function(req, res) {
    usuarios.login(req, res)
  })
  
  //Hace logout de un usuario
  app.post('/apitechu/v5/logout', function(req, res) {
    usuarios.logout(req, res)
  })
}