
var requestJson = require('request-json')
var urlMlabRaiz = "https://api.mlab.com/api/1/databases/techu2018/collections"
var apiKey = "apiKey=1fa5CiAnKeIRiPcw8OVs9zOcvFJZOQvn"

exports.listaUsuarios = function(req, res) {
    var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
    clienteMlab.get('', function(err, resM, body) {
      if (!err) {
        res.send(body)
      }
    })
}

exports.obtenerUsuario = function(req, res) {
    console.log(req.params.id)
    var id = req.params.id
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios/" + id + "?" + apiKey)
    clienteMlab.get('', function(err, resM, body) {
      if (!err) {
        delete body['password']
        res.send(body)
      }
    })
}

exports.crearUsuario = function(req, res){
    var data = {
        nombre: req.body.nombre,
        apellidos: req.body.apellidos,
        password: req.body.password,
        email: req.body.email,
        logged: false
      }
      console.log(JSON.stringify(data))
      clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
      clienteMlab.post("?"+ apiKey, data,  function(errP, resP, bodyP) {
        res.send(bodyP)
      })
}

exports.modificarUsuario = function(req, res) {
    var data = {
        $set: {
          nombre: req.body.nombre,
          apellidos: req.body.apellidos
        }
      }
      var id = req.body.id;
    
      clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios/" + id + "?" + apiKey)
      clienteMlab.put('', data,  function(errP, resP, bodyP) {
        res.send(bodyP)
      })
}

exports.eliminarUsuario = function(req, res) {
    var _id = req.params._id
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios/"+ _id + "?" + apiKey)
    clienteMlab.del('', function(err, resM, body) {
      res.send(body)
    })
}

exports.login = function(req, res) {

  var email = req.headers.email
  var password = req.headers.password
  var query = 'q={"email":"' + email + '","password":"' + password + '"}'

  console.log("Haciendo login: " + query)
  
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length == 1) // Login ok
      {
        var data = {
          $set: {
            logged: true
          }
        }
        clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
        var query = "?q={'email':'" + email + "'}&"
        clienteMlab.put( query + apiKey, data,  function(errP, resP, bodyP) {
          delete body[0]['password']
          res.send(body[0])
        })
      }
      else {
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
}

exports.logout = function(req, res){
    var id = req.headers.id
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
    var data = {
        $set: {logged: false}
    }
    var query = "?q={'_id':'" + id + "'}&"
    clienteMlab.put(query + apiKey, data, function(errP, resP, body) {
        res.send(body)        
    })
}