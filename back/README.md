# Proyecto Back TechU 2018
### Cristóbal Sepúlveda

Conjunto de APIs desarrolladas en express para exponer datos de usuarios y cuentas y funcionalidades como Login, Logout y otras funcionalidades requeridas para el proyecto final del curso Techu2018.  Estas APIs acceden a los datos almacenados en mlab, mediante el API que mlab proporciona.

# Comenzando

- Clonar repositorio
- `npm install` para instalar las dependencias
- `npm start`para arrancar la API
- `npm test` para lanzar las pruebas unitarias.

# Código

## Dependencias

- [chai y chai-http](http://chaijs.com/), para realizar pruebas unitarias de APIs
- [mocha](https://mochajs.org/), framework que permite la realización de pruebas unitarias javascript.
- [nodemon](https://nodemon.io/), monitoriza los cambios en el código, reiniciando de forma automática el servidor, agilizando así las pruebas de la aplicación durante el desarrollo.
- [express](http://expressjs.com), es un framework para el desarrollo de aplicaciones de servidor con node.
- [request-json](https://www.npmjs.com/package/request-json), para realizar llamadas a API REST usando Json.

## Estructura del código

- test, directorio donde se encuentran los archivos de pruebas unitarias.
- server.js, contiene el código que implementa y expone las APIs
- package.json, contiene todas las depenencias del proyecto
- Dockerfile, permite lanzar las aplicación en un contenedor Docker




