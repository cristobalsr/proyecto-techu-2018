var express = require('express')
var bodyParser = require('body-parser')
var cors = require('cors')

var app = express()
const TokenGenerator = require('uuid-token-generator');
const tokgen2 = new TokenGenerator(256, TokenGenerator.BASE62);
var tablaTokens = {}

app.use(cors())
var requestJson = require('request-json')
var clienteMlab
var port = process.env.PORT || 4000
var proxy = require('express-http-proxy');


app.use('/', proxy('localhost:3000', {
  filter: function(req, res) {
    console.log(req.originalUrl)
    if ((req.originalUrl == "/apitechu/v5/login") || (req.originalUrl == "/apitechu/v5/usuarios")) {
      return true
    }

    if ((req.headers.token != "") && (req.headers.usuario != "")) {
      var usuario = req.headers.usuario
      var token = req.headers.token
      if (usuario in tablaTokens ){
        if(tablaTokens[usuario].token == token) {
          console.log("Acceso correcto para usuario:" + usuario + "con token: " + token)
          return true
        }
      }
      console.log("Acceso no permitido para usuario:" + usuario + "con token: " + token)
      return false
    }
    return false
  },

  userResDecorator: function(proxyRes, proxyResData, userReq, userRes) {
    var data = JSON.parse(proxyResData.toString('utf8'));
    console.log(data)
    if (userReq.originalUrl == "/apitechu/v5/login"){
      var tokenGenerado = tokgen2.generate();
      var usuario = data._id.$oid
      tablaTokens[usuario]={token: tokenGenerado}
      
      data.token = tokenGenerado;
      console.log(data)
      return JSON.stringify(data);
    }
    else {
      console.log(JSON.stringify(data))
      return JSON.stringify(data);
    }
  }


}));




app.listen(port)

console.log("API escuchando en el puerto" + port)