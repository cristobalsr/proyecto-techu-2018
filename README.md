# Proyecto TechU 2018

### Cristóbal Sepúlveda

Proyecto desarrollado para el curso Practitioner del TechU 2018.  Consiste en una aplicación
cliente-servidor, en la cual el cliente está basado en Polymer y el Back en nodejs.
El Back consta de dos partes: un API con la lógica de acceso a los datos y un Gatway para controlar
el acceso al API, proporcionando requisitos mínimos de seguridad, mediante autenticación a través de
tokens.

# Comenzando

Dentro de cada directorio, "back", "front" y "gateway" están las especificaciones necesarias para poder instalarlo.

# Procesos

Mediante PM2 se lanzan los procesos de backend, es decir, el gateway y el API.  PM2 permite gestionar
y monitorizar dichos procesos para diferentes entornos, pudiendo controlar el número de instancias
y el balanceo de carga entre las mismas.  Todo es configurado mediante el fichero process.yml, en el cual se indican los scripts y los parámetros para lanzar los mismos.
http://pm2.keymetrics.io/docs/usage/application-declaration/

