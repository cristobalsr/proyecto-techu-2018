# Techu2018 Front  

Aplicación Front desarrollada con Polymer 2.0 para el proyecto TechU edición 2018.
Alumno: Cristóbal Sepúlveda


## Instalación Polymer-CLI

First, make sure you have the [Polymer CLI](https://www.npmjs.com/package/polymer-cli) installed. Then run `polymer serve` to serve your application locally.

## Para visualizar la aplicación

```
$ polymer serve --open
```

## Compilar la aplicación

```
$ polymer build
```

This will create builds of your application in the `build/` directory, optimized to be served in production. You can then serve the built versions by giving `polymer serve` a folder to serve from:

```
$ polymer serve build/default
```

## Para lanzar los Tests

```
$ polymer test
```

Your application is already set up to be tested via [web-component-tester](https://github.com/Polymer/web-component-tester). Run `polymer test` to run your application's test suite locally.
